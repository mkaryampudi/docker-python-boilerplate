#!/bin/bash

BUILD_URL=${REGISTRY_BASEURL}/${IMAGE_NAME}
aws s3 cp s3://${S3_BUCKET}/prod.env .env
docker-compose up --no-start
docker tag ${IMAGE_NAME}:latest ${BUILD_URL}:${TAG}
docker push ${BUILD_URL}:${TAG}
