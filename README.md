# Docker Development Environment for Python (in Visual Studio Code)

This is a proof-of-concept for developing locally using Docker in VSCode. The benefits of developing in docker:

- No need to install dependencies on your local machine - dependencies are only installed in Docker container
- We can load environment variables into the container using docker compose
- Running in docker container that is similar to production makes it easier to catch issues that would only arise after building the image

Prerequisites:

- Docker
- Visual Studio Code
- Python

Instructions:

1. Clone this repo to a local directory, and use the Terminal to `cd` to the project directory
1. Initialize a project-specific Python environment: `python -m venv venv`
1. In Visual Studio Code, install the `Python` extension
1. In Visual Studio Code, open the `Integrated Terminal` (`Ctrl + ~`). This should automatically activate the Python virtual environment created in a previous step.
1. In the Integrated Terminal, install `pylint`: `pip install pylint`
1. Add a `.env` file to the root of the project directory
1. Use Docker Compose to build the Docker image and run it: `docker-compose up`
1. To change or set environment variables, modify the `.env` file

## Debugging in Visual Studio Code:

- Open the command palette (`Command + Shift + P`) and search for `launch.json`.
- Add the following to `launch.json`:

```
        {
            "name": "Python: Attach (Docker Debug)",
            "type": "python",
            "request": "attach",
            "pathMappings": [
                {
                    "localRoot": "${workspaceFolder}",
                    "remoteRoot": "/usr/src/app/"
                }
            ],
            "port": 5678,
            "host": "localhost"
        }
```

- Add `DEBUG=true` to the `.env` file to enable debugging
- Restart the Docker container
- Set a breakpoint anywhere in `app.py`
- Make a `GET` request to `http://localhost:3000/hello/world`. The code should halt at the breakpoint and you should be able to step through the code and inspect variables

## Building container for production

- The best practice for handling secrets in production is to store a production `.env` file in AWS S3, and then
fetching the `.env` file using the `aws-cli` command. Create and save a production `.env` in an S3 bucket.
- Install `awscli` pip package: `pip install awscli`
- Run `docker-build-push.sh` in a build pipeline such as Bitbucket Pipelines or Jenkins. Ensure that the `REGISTRY_BASEURL` environment variable is set; this is the URL to the Docker repository (such as AWS ECR or Docker Repository). Also ensure that the `S3_BUCKET` environment variable is set.

```
IMAGE_NAME=example_app TAG=latest sh docker-build-push.sh
```
