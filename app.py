import dotenv
import os
import ptvsd
from flask import Flask
from flask_restful import Api, Resource

dotenv.load_dotenv()

app = Flask(__name__)
api = Api(app)


def hello(name=None):
    if not name:
        return 'Hello World!!!'

    return f'Hello {name}!'


def init_ptvsd():
    debug = os.getenv('DEBUG', False)
    if debug and debug == 'True':
        ptvsd.enable_attach(address=('0.0.0.0', 5678))


class TestResource(Resource):
    def get(self, name):
        return hello(name)


if __name__ == '__main__':
    init_ptvsd()
    api.add_resource(TestResource, '/hello/<string:name>')
    app.run(host='0.0.0.0', port=3000)
