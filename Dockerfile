FROM python:3.6.4-alpine3.7

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip3 install -r requirements.txt
COPY .env ./
COPY app.py ./

EXPOSE 3000
EXPOSE 5678
CMD python app.py
